* Caso práctico - EN - Python
** Code Challenge Description

In Reservamos we're always looking for ways to help our users have the
best experience when looking for travel  options so that they can make
the best decision when reserving a bus or flight ticket. To accomplish
this, we have to make a REST API which can be consumed by a client app
to  check  the weather  forecast  for  the destinations  available  in
Reservamos.  The API's  functionality is  to  be able  to compare  the
weather  forecast  for   the  next  7  days,  by   day,  of  different
destinations offered  by Reservamos.  The client app  must be  able to
send the name of a city  and fetch the maximum and minimum temperature
for these places.

For this challenge you will be  using OpenWeather's API to look up the
temperature  using geographic  coordinates,  and show  the results  in
metric units.

+ Docs: [[https://openweathermap.org/api/one-call-api][link]]
+ API key: ----

To find the coordinates of a given city, you will use an endpoint from
the Reservamos API where we can get data for different places by name.

+ Docs: [[https://documenter.getpostman.com/view/6904537/TzRRCo6f][link]]
+ No API key required
** Expected output:

An endpoint that receives a city name and return a list of cities with
it’s weather forecast:
*** Params
Partial or total name of any city in Mexico, e.g: Mon or Monterrey
*** Results
List of  cities that match the  given param including the  maximum and
minimum  temperature for  the next  7 days  (include only  cities into
results)
* How to use the API

1. Download the project from: [[https://gitlab.com/mildewyPrawn/reservamos-api-test][gitlab]].
2. Make sure to install the requeriments.txt
   #+BEGIN_SRC bash
     conda install --file requirements.txt
   #+END_SRC

3. Run project:
   #+BEGIN_SRC bash
     cd reservamos-api-test
     python manage.py runserver
   #+END_SRC
4. Go to: http://127.0.0.1:8000/api/search_by_name/?city=mon

   where =mon= is the partial or total name of any city in Mexico.
5. Enjoy :)
