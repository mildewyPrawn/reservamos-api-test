from django.urls import path, reverse

from . import views

urlpatterns = [
    path(r'search_by_name/', views.search_view, name='search_view'),
]
