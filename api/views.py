from django.http import HttpResponse, JsonResponse
import requests
import json

api = "a5a47c18197737e8eeca634cd6acb581"
'''query for get lat and long'''
query_places = "https://search.reservamos.mx/api/v2/places?q="
'''query for get temperature from lat and long'''
query_weather = "https://api.openweathermap.org/data/2.5/onecall?lat={}&lon={}&exclude=current,minutely,hourly,alerts&appid={}"

'''
Return the json response with the specified data.
'''
def search_view(request):
    query = request.GET.get('city', 'mon')
    response_query = requests.get(query_places + query)
    lat_lon_query = transform_lats_lons(response_query.json())
    len_q = len(lat_lon_query)
    return JsonResponse({'cities': lat_lon_query})

'''
Return the data diven the string to search.
'''
def transform_lats_lons(query):
    new = []
    for q in query:
        if q['result_type'] == 'city':
            max_min = temperature_data = temperature(q['lat'], q['long'], api)
            d = {'name' : q['display'], 'lat': q['lat'], 'long': q['long']}
            for i in range(0,7):
                max_min_list = {'max' : max_min['maxs'][i], 'min': max_min['mins'][i]}
                d['max_min_' + str(i)] = max_min_list
            new.append(d)
    return new

'''
Return max and min temperature for the next 7 days for the city in (lat,long)
'''
def temperature(lat, long, api):
    maxs = []
    mins = []
    response_data_t = requests.get(query_weather.format(lat, long, api)).json()
    for i in range(0,7):
        mins.append(response_data_t['daily'][i+1]['temp']['min'])
        maxs.append(response_data_t['daily'][i+1]['temp']['max'])
    return {'maxs' : maxs, 'mins' : mins}
